/**
 * TODO: create an input component here, emit text when user type in text
 */

import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {EmployeeService} from '../../services/employee/employee.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit{
  name: FormControl = new FormControl();

  constructor(private employeeService: EmployeeService) {
  }


  ngOnInit() {
    this.name.valueChanges
      .pipe(debounceTime(200))
      .pipe(distinctUntilChanged())
      .subscribe( res => this.employeeService.setKeyword(res));
  }

}


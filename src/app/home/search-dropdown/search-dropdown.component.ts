/**
 * TODO: implement a dropdon component here and hook up with search input from homepage,
 * 1. whenever search input changes, the dropdown will only show employees with his/her name
 * containing the search text
 * 2. employee item in dropdown should be clickable, clicking makes current employee to be selected
 */
import {Component, DoCheck, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {EmployeeService} from '../../services/employee/employee.service';
import {Store} from '@ngrx/store';
import {getDisplayEmployee} from '../../store/actions';

@Component({
  selector: 'search-dropdown',
  templateUrl: './search-dropdown.component.html',
  styleUrls: ['./search-dropdown.component.scss']
})
export class SearchDropDownComponent implements OnInit, DoCheck{
  private employees: any[] = [];
  private keyword = '';
  private results: any[] = [];


  constructor(private employeeService: EmployeeService,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.employeeService.getEmployee()
      .subscribe(res => {
        this.employees = res.data;
      });
  }

  ngDoCheck(): void {
    this.results = [];
    this.employeeService.getKeyword().subscribe(res => this.keyword = res);
    if (this.keyword !== '') {
      const searchRegex = new RegExp( '' + this.keyword + '', 'i');

      for (const employee of this.employees) {
        if ( searchRegex.test(employee.employee_name) === true) {
          this.results = this.results.concat([employee]);
        }
      }
    }
  }

  setDisplayEmployee(employee) {
    // this.employeeService.setDisplayEmployee(employee);
    this.store.dispatch(getDisplayEmployee({employee}));
  }
}

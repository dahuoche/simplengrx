import {Component, DoCheck, OnInit} from '@angular/core';
import {EmployeeService} from '../services/employee/employee.service';
import {Store} from '@ngrx/store';

 /**
  * TODO:
  * 1. Integrate with employee service
  * 2. Show selected employee card when an employee is selected from the dropdown
  * 3. Try to fire an action when an employee get selected, and read the selected
  * employee information from application state
  */

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, DoCheck {
   private employees: any[];
   displayEmployee: any = {};
   constructor(private employeeService: EmployeeService,
               private store: Store<any> ) {
   }

   ngOnInit(): void {
     this.employeeService.getEmployee().subscribe(res => this.employees = res.data);
   }

   // ngDoCheck() {
   //    this.employeeService.getDisplayEmployee().subscribe(res => this.displayEmployee = res);
   // }

   ngDoCheck() {
     this.store.subscribe(state => this.displayEmployee = state['employee'].employee);
     console.log(this.displayEmployee);
   }

 }


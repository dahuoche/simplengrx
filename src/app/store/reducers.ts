/**
 * TODO:
 * 1. Build a Reducer and name it selectEmployeeReducer to match with the name in app.module.ts
 * 2. Uncomment the imports for StoreModule inside AppModule
 */
import {AppState} from './types/appState.type';
import {createReducer, on} from '@ngrx/store';
import * as HomeAction from './actions';

export const initialAppState: AppState = {
  employee: undefined
};

export const selectEmployeeReducer =  createReducer(
  initialAppState,
  on(HomeAction.getDisplayEmployee, (state, action) => {
    return {
      employee: action.employee
    };
  })

);

/**
 * TODO: Create Action for Selecting an Employee
 */
import {createAction, props} from '@ngrx/store';

export const getDisplayEmployee = createAction(
  '[Home Page] Get Display Employee',
  props<{employee: any}>()
);

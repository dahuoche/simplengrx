/**
 * TODO: create injectable service to fetch all employees imformation from below url:
 * http://dummy.restapiexample.com/api/v1/employees
 * You can use class HttpService from `./http-wrapper` folder to simplify your http request
 */
import {HttpService} from '../http-wrapper/http.service';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';



@Injectable({providedIn: 'root'})
export class EmployeeService {
  private keyword: string;
  private displayEmployee;

  constructor(private http: HttpService) {}


  getEmployee(): Observable<any> {
    return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }

  setKeyword(name: string) {
    this.keyword = name;
  }

  getKeyword(): Observable<string> {
    return new Observable<string>((obs) => obs.next(this.keyword));

  }


  setDisplayEmployee(employee: any) {
    this.displayEmployee = employee;
  }

  getDisplayEmployee() {
    return new Observable<any>((obs) => obs.next(this.displayEmployee));
  }
}

